// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const dashboards = 'dashboards';
const authentication = 'authentication';

export const environment = {
  production: false
};

// export const apiUrl = 'https://reports.rangemod.com/api/v1/';
// export const dashboardId = '5cab9893e256cd0e4ca3b3ea';

export const apiUrl = 'http://main.mytestapp.com:8083/api/v1/';
export const dashboardId = '5d305ff9bd3e7b3ff06765c8';

export const apiDashboards = {
  getAllWidgets: apiUrl + dashboards + '/' + dashboardId + '/widgets',
};

export const apiAuthentication = {
  login: apiUrl + authentication + '/login',
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
