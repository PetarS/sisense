import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ApiService } from '@app/services/api.service';

const DECLARATIONS = [
  
];

const IMPORTS = [
  CommonModule,
  ReactiveFormsModule,
  FormsModule 
];

const PROVIDERS = [
  ApiService
];

@NgModule({
  declarations: [...DECLARATIONS],
  imports: [...IMPORTS],
  exports: [...DECLARATIONS, ...IMPORTS],
  providers: [...PROVIDERS ]
})
export class SharedModule { }
