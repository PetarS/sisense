import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ApiService {

  constructor(private http: HttpClient) { }

  // General GET method
  get = (url, data?): any => {
    return this.http.get(url, data);
  }

  // General POST method
  post = (url, data?): any => {
    return this.http.post(url, data);
  }

  // General DELETE method
  delete = (url, id?): any => {
    return this.http.delete(url, id);
  }
}
