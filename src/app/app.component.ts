import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiService } from './services/api.service';
import { apiDashboards, apiAuthentication } from '../../src/environments/environment';
declare let Sisense, prism: any; //declare moment

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'sisense';


  loginObj = {
    username: 'mike.caruso@deluxe.com',
    password: 'printing11'
  }

  constructor(
    private http: HttpClient,
    private apiService: ApiService,
  ) { }

  ngOnInit() {
    // this.getCookies();
    // this.login();
    // this.ssoSettings();
    console.log(Sisense);
    // console.log(prism);

    // this.test();
    
    // var widgets = prism.activeDashboard.widgets;
    // for (var i=0; i< widgets.length; i++) {
    //   console.log(prism.activeDashboard.widgets.$$widgets[i].title)+ "\n"
    //   console.log(prism.activeDashboard.widgets.$$widgets[i].type)+ "\n"
    //   console.log(prism.activeDashboard.widgets.$$widgets[i].oid)+ "\n"
    //   console.log('JAQL')
    //   console.log(prism.debugging.GetJaql(prism.activeDashboard.widgets.$$widgets[i]))
    //   console.log('SQL')
    //   console.log(prism.debugging.GetSql(prism.activeDashboard.widgets.$$widgets[i]))
    //   console.log(' '); 
    // };
    
  }

  // private ssoSettings = () => {
  //   this.apiService.get('https://reports.rangemod.com/api/v1/settings/sso').subscribe(response => {
  //     console.log(response);
  //   });
  // };

  // private getCookies = () => {
  //   let allCookies = document.cookie;
  //   console.log(allCookies);
    
  // }

  private login = () => {
    this.apiService.post(apiAuthentication.login, this.loginObj).subscribe(response => {
      console.log(response);
    });
  };

  public test = () => {
    console.log('aaa');
    
    Sisense.connect('http://reporting.mytestapp.com:8081').then(function (app) { // replace with your Sisense server address
      console.log(app);

          app.dashboards.load('5d305ff9bd3e7b3ff06765c8').then(function (dash) { //replace with your dashboard id
              console.log(dash);
              console.log(dash.widgets.get('5d30600cbd3e7b3ff06765cb'))
              dash.widgets.get('5d30600cbd3e7b3ff06765cb').container = document.getElementById("widget1"); //replace with one of your widgets' id.
              dash.widgets.get('5d309053e6ddbb21cc059982').container = document.getElementById("widget2"); //replace with one of your widgets' id.
              dash.refresh();
          });
          // console.log(prism.activeDashboard);
          console.log(prism);
      });
  }

  // private getAllWidgets = () => {
  //   this.apiService.get(apiDashboards.getAllWidgets).subscribe(response => {
  //     console.log(response);
  //   });
  // };

  // private hitSomething() {
  //   return this.http.get("http://localhost:8081/app/main#/dashboards/5d305ff9bd3e7b3ff06765c8/?filters=all");
  // }
}
